/*
 * Controlling a 16 Channel Multiplexer
 * with GPIO Lines
 */

use crate::output_pin::UpdatePinState;


pub struct Multiplexer16<'a> {
    pins: [&'a mut dyn UpdatePinState; 4],
}


impl<'a> Multiplexer16<'a> {

    pub fn new(pins: [&mut dyn UpdatePinState; 4]) -> Multiplexer16 {
        Multiplexer16 {
            pins: pins,
        }
    }

    pub fn select(&mut self, channel: u8) -> Result<u32, &str> {
        match channel {
            0..=15 => {
                self.pins[0].set_state(channel & 0b0001 > 0);
                self.pins[1].set_state(channel & 0b0010 > 0);
                self.pins[2].set_state(channel & 0b0100 > 0);
                self.pins[3].set_state(channel & 0b1000 > 0);
                return Ok(0);
            },
            _ => return Err("Value outside of range"),
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use mockall::*;

    struct OutputPinM{
        state: bool,
    }
    #[automock]
    impl UpdatePinState for OutputPinM {
        fn set_state(&mut self, state: bool) {
            self.state = state;
        }
    }

    #[test]
    fn test_multiplexer() {

        let vpin0 = &mut MockOutputPinM::new();
        let vpin1 = &mut MockOutputPinM::new();
        let vpin2 = &mut MockOutputPinM::new();
        let vpin3 = &mut MockOutputPinM::new();

        vpin0.expect_set_state().times(16).return_const(());
        vpin1.expect_set_state().times(16).return_const(());
        vpin2.expect_set_state().times(16).return_const(());
        vpin3.expect_set_state().times(16).return_const(());

        let mut mult = Multiplexer16::new(
           [
               vpin0,
               vpin1,
               vpin2,
               vpin3,
           ]
        );

        for i in 0..=15 {
            mult.select(i).unwrap();
        }

        let result = mult.select(16);
        assert!(result.is_err());
    }
}
