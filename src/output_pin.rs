use linux_embedded_hal::gpio_cdev::{Chip, LineHandle, LineRequestFlags};


pub struct OutputPin {
    line_handle: LineHandle,
}


pub trait UpdatePinState {
    fn set_state(&mut self, state: bool);
}

impl OutputPin {
    pub fn new(chip: &str, line: u32, name: &str, additional_flags: LineRequestFlags) -> OutputPin {
        let mut chip = Chip::new(chip).unwrap();
        let line_handle = chip
            .get_line(line)
            .unwrap()
            .request(LineRequestFlags::OUTPUT | additional_flags, 1, name)
            .unwrap();
        OutputPin {
            line_handle: line_handle,
        }
    }
}

impl UpdatePinState for OutputPin {
    fn set_state(&mut self, state: bool) {
        self.line_handle
            .set_value(if state { 1 } else { 0 })
            .unwrap();
    }
}
