use ads1x1x::{Ads1x1x, DataRate16Bit, SlaveAddr, channel};
use embedded_hal::adc::OneShot;
use linux_embedded_hal::I2cdev;
use nb::block;
use linux_embedded_hal::gpio_cdev::LineRequestFlags;

//use influxdb::{Client, Query, Timestamp, ReadQuery};
//use influxdb::InfluxDbWriteable;
//use chrono::{DateTime, Utc};

//use multiplex::OutputPin;
//use crate::multiplex::Gnampf;


mod output_pin;
mod multiplex;

use output_pin::OutputPin;
use multiplex::Multiplexer16;



fn main() {
    let pin0 = &mut OutputPin::new("/dev/gpio-0", 0, "ADC Multiplexer0", LineRequestFlags::empty());
    let pin1 = &mut OutputPin::new("/dev/gpio-0", 1, "ADC Multiplexer1", LineRequestFlags::empty());
    let pin2 = &mut OutputPin::new("/dev/gpio-0", 2, "ADC Multiplexer2", LineRequestFlags::empty());
    let pin3 = &mut OutputPin::new("/dev/gpio-0", 3, "ADC Multiplexer3", LineRequestFlags::empty());
    let dev = I2cdev::new("/dev/i2c-1").unwrap();



    let mut adc = Ads1x1x::new_ads1115(dev, SlaveAddr::default());
    let mut multiplexer = Multiplexer16::new([pin0, pin1, pin2, pin3]);
    adc.set_data_rate(DataRate16Bit::Sps860).unwrap();

    for mux in 0..=15 {
        multiplexer.select(mux).unwrap();
        let values = [
            block!(adc.read(&mut channel::SingleA0)).unwrap(),
            block!(adc.read(&mut channel::SingleA1)).unwrap(),
            block!(adc.read(&mut channel::SingleA2)).unwrap(),
            block!(adc.read(&mut channel::SingleA3)).unwrap(),
        ];
    }



    // let mut chip = Chip::new("/dev/gpiochip0").unwrap();
    // let s0 = chip.get_line(0).unwrap().request(LineRequestFlags::OUTPUT, 1, "s0").unwrap();
    // let s1 = chip.get_line(1).unwrap().request(LineRequestFlags::OUTPUT, 1, "s1").unwrap();
    // let s2 = chip.get_line(2).unwrap().request(LineRequestFlags::OUTPUT, 1, "s2").unwrap();
    // let s3 = chip.get_line(3).unwrap().request(LineRequestFlags::OUTPUT, 1, "s3").unwrap();


    // let client = Client::new("http://localhost:8086", "test");


    // #[derive(InfluxDbWriteable)]
    // struct WeatherReading {
    //     time: DateTime<Utc>,
    //     humidity: i32,
    //     #[influxdb(tag)] wind_direction: String,
    // }

    // // Let's write some data into a measurement called `weather`
    // let weather_readings = vec!(
    //     WeatherReading {
    //         time: Timestamp::Hours(1).into(),
    //         humidity: 30,
    //         wind_direction: String::from("north"),
    //     }.into_query("weather"),
    //     WeatherReading {
    //         time: Timestamp::Hours(2).into(),
    //         humidity: 40,
    //         wind_direction: String::from("west"),
    //     }.into_query("weather"),
    // );


    // let write_result = client
    //     .query(weather_readings)
    //     .await;


}
