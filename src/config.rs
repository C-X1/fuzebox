use std::fs::File;
use std::io::Read;
use serde::Deserialize;
use std::path::PathBuf;

use crate::output_pin::{OutputPin, UpdatePinState};
use linux_embedded_hal::gpio_cdev::LineRequestFlags;



#[derive(Debug, Deserialize, Default, PartialEq)]
pub struct GPIOPinConfig {
    chip: String,
    pin: u32,
    #[serde(default = "default_false")]
    active_low: bool,
    #[serde(default = "default_false")]
    open_drain: bool,
    #[serde(default = "default_false")]
    open_source: bool,
}

fn default_false() -> bool {
    false
}


#[derive(Debug, Deserialize, PartialEq)]
pub struct Config {
    multiplex_pins: [GPIOPinConfig; 4],
    adc_interface: String,
}

pub fn config_from_file(path: PathBuf) -> Config {
    let mut file = File::open(path).unwrap();
    let mut yaml_content = String::new();
    file.read_to_string(&mut yaml_content).unwrap();

    return serde_yaml::from_str(&yaml_content).unwrap();
}

pub fn pins_from_config(config: &Config) -> Vec::<OutputPin> {
    let multiplex_pins = &config.multiplex_pins;

    let mut pins = Vec::<OutputPin>::new();

    for (index, pin_config) in multiplex_pins.iter().enumerate() {
        let mut flags = LineRequestFlags::empty();

        if pin_config.active_low {
            flags |= LineRequestFlags::ACTIVE_LOW;
        }

        if pin_config.open_drain {
            flags |= LineRequestFlags::OPEN_DRAIN;
        }

        if pin_config.open_source {
            flags |= LineRequestFlags::OPEN_SOURCE;
        }

        let pin_usage = "FuzeBox Multiplexer S".to_owned() +
            &index.to_string();

        pins.push(
            OutputPin::new(
                pin_config.chip.as_str(),
                pin_config.pin,
                pin_usage.as_str(),
                flags
            )
        );
    }

    return pins;
}


#[cfg(test)]
mod tests {
    use super::*;


     #[test]
    fn test_config_from_file() {
        let crate_dir = env!("CARGO_MANIFEST_DIR");
        let config_path =
            PathBuf::from(crate_dir)
            .join("resources")
            .join("config.yaml");

        let config: Config = config_from_file(config_path);

        assert_eq!(config,
                   Config {
                       multiplex_pins: [
                           GPIOPinConfig {
                               chip: "/dev/gpio-bogus0".to_owned(),
                               pin: 3001,
                               active_low: true,
                               open_drain: false,
                               open_source: false,
                           },
                           GPIOPinConfig {
                               chip: "/dev/gpio-bogus1".to_owned(),
                               pin: 3002,
                               active_low: false,
                               open_drain: true,
                               open_source: false,
                           },
                           GPIOPinConfig {
                               chip: "/dev/gpio-bogus2".to_owned(),
                               pin: 3003,
                               active_low: false,
                               open_drain: false,
                               open_source: true,
                           },
                           GPIOPinConfig {
                               chip: "/dev/gpio-bogus3".to_owned(),
                               pin: 3004,
                               active_low: false,
                               open_drain: false,
                               open_source: false,
                           },
                       ],
                       adc_interface: "/dev/spi-bogus0".to_owned()
                   });
    }

}
